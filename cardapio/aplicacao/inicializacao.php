<?php  global $configuracao; ?>

<!-- FONTS -->
<link href="https://fonts.googleapis.com/css?family=EB+Garamond:400,400i,500,500i,600,600i,700,700i,800,800i|Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

<!-- CSS -->

<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/css/animate.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/jquery.fancybox.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-buttons.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-thumbs.css" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/cardapio/css/cardapio.css" />


<!-- JS -->
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/jquery.fancybox.pack.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-buttons.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-media.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/galeria/source/helpers/jquery.fancybox-thumbs.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/cardapio/js/cardapio.js"></script>


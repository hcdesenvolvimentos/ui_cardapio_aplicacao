
$(function(){
 	
 	
	$(".cardapioVictor a").click(function(e) {
	
		event.preventDefault();
		$( ".loadCardapio" ).fadeIn(800);
		$( "#floatingCirclesG" ).show();
		setTimeout(function(){ 
			$( ".cardapioInicial" ).addClass('cardapioInicialSlideIn');
			$( ".cardapioInicial" ).removeClass('cardapioInicialSlideOut');
			$( ".cardapioInicial" ).show();
		}, 3000);
				
	});

	$(".cardapioPetiscaria a").click(function(e) {
	
		event.preventDefault();
		$( ".loadCardapio" ).fadeIn(800);
		$( "#floatingCirclesG" ).show();
		setTimeout(function(){ 
			$( ".cardapioInicial" ).addClass('cardapioInicialSlideIn');
			$( ".cardapioInicial" ).removeClass('cardapioInicialSlideOut');
			$( ".cardapioInicial" ).show();
		}, 3000);
				
	});

	$( "#fecharInicial" ).click(function() {
		$(".nomeCategoria").slideUp();
		$( ".cardapioInicial" ).addClass('cardapioInicialSlideOut');
		$( ".cardapioInicial" ).removeClass('cardapioInicialSlideIn');
		$( "#floatingCirclesG" ).hide();
		setTimeout(function(){ 
			$( ".cardapioInicial" ).hide();
			$( ".loadCardapio" ).fadeOut(300);

		}, 1000);
	});


	$( ".cardapioInicial .sidebar aside nav h3" ).click(function() {
		$(".cardapioInicial .sidebar aside nav h3").removeClass('ativo');
		//$(this).addClass('ativo');
		var slug = $(this).attr('data-slug');
		$("."+slug).show();
		$("."+slug).addClass('mostrarCardapioSlideIn');
		$("."+slug+" .contentCardapioSidebar").addClass('SidebarFadeIn');
		 
	});

	$( ".cabecalhoCardapio" ).click(function() {
		$( ".efeitoFade .contentCardapioDescripition" ).addClass('descripitionFadeOut');
		$( ".efeitoFade .contentCardapioSidebar" ).addClass('SidebarFadeout');
		$( ".efeitoFade" ).addClass('CardapioPrincipalFadeOut');
		setTimeout(function(){ 
			$( ".efeitoFade" ).hide();
			$( ".efeitoFade .contentCardapioDescripition" ).removeClass('descripitionFadeOut');
			$( ".efeitoFade .contentCardapioSidebar" ).removeClass('SidebarFadeout');
			$( ".efeitoFade" ).removeClass('CardapioPrincipalFadeOut');
		}, 700);
		setTimeout(function(){ 
		    $( "a.funcaoVoltarTopo" ).trigger( "click" );			
		}, 1000);

	});


	$(".fecharPrincial").click(function(e) {
		$(".nomeCategoria").slideUp();
		$( ".efeitoFade .contentCardapioDescripition" ).addClass('descripitionFadeOut');
		$( ".efeitoFade .contentCardapioSidebar" ).addClass('SidebarFadeout');
		$( ".efeitoFade" ).addClass('CardapioPrincipalFadeOut');
		setTimeout(function(){ 
			$( ".efeitoFade" ).hide();
			$( ".efeitoFade .contentCardapioDescripition" ).removeClass('descripitionFadeOut');
			$( ".efeitoFade .contentCardapioSidebar" ).removeClass('SidebarFadeout');
			$( ".efeitoFade" ).removeClass('CardapioPrincipalFadeOut');
		}, 700);
		setTimeout(function(){ 
		    $( "a.funcaoVoltarTopo" ).trigger( "click" );			
		}, 1000);

	});


	$('a.scroll').click(function(e) {
		
		let nomeCategoria = $(this).children().text();
		let css = $(this).children().attr('data-color');
		let idSessao = $(this).children().attr('data-id');
		console.log(idSessao);

		$('.nomeCategoria').text(nomeCategoria);
		$('.nomeCategoria').css('color',css);
		setTimeout(function(){ 
			$(".nomeCategoria").fadeIn();
		}, 1000);
		$("#fecharPrincial").hide();
		setTimeout(function(){ 
			$("#fecharPrincial").show(200);
		}, 1000);

		$(".mostrarCardapio .contentCardapioSidebar .itemSidebar h2").removeClass('ativo');
		$(this).children('h2').addClass('ativo');
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
		
		
		
	});

	

	$('a.cabecalhoCardapio').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000);
				return false;
			}
		}
		
	});

	$("a#example4").fancybox({
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true
	});



		
    $('a.funcaoVoltarTopo').click(function () {	 
    	setTimeout(function(){ 
			$(".nomeCategoria").slideUp();
		}, 1000);
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 400);
	        return false;
	      }
	    }
    });

   	$(window).bind('scroll', function () {
	   var alturaScroll = $(window).scrollTop();
	    if(alturaScroll > 700){
	    	$('a.funcaoVoltarTopo').slideDown();
	    }else{
	    	$('a.funcaoVoltarTopo').slideUp();
	    }
	});



});


